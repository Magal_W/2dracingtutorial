﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSpawner : MonoBehaviour {

	public GameObject[] cars;
	public float spawnTime = 1f;
	float timer;
	int carNumber;

	// Use this for initialization
	void Start () {
		timer = spawnTime;
	}
	
	// Update is called once per frame
	void Update () {
		timer -= Time.deltaTime;
		if(timer <= 0)
		{
			carNumber = Random.Range(0, 5);
			Vector3 carPos = new Vector3(Random.Range(-2.1f, 2.1f), transform.position.y, transform.position.z);
			Instantiate(cars[carNumber], carPos, transform.rotation);
			timer = spawnTime;
		}
	}
}
