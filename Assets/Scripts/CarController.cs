﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour {

	public UIManager uiManager;
	public AudioManager audioManager;
	public float carSpeed;
	Vector3 position;
    bool isAndroidPlatform = false;
    Rigidbody2D rigidBody;

	void Awake() {
        rigidBody = GetComponent<Rigidbody2D>();
#if UNITY_ANDROID
        isAndroidPlatform = true;
#else
        isAndroidPlatform = false;
#endif
        audioManager.carSound.Play();
	}
	
	// Use this for initialization
	void Start () {
		position = transform.position;
        if(isAndroidPlatform)
        {
            Debug.Log("Android");
        }
        else
        {
            Debug.Log("Windows");
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (isAndroidPlatform)
        {
            TouchMove();
        }
        else
        {
            position.x += Input.GetAxis("Horizontal") * carSpeed * Time.deltaTime;
        }
        position = transform.position;
        position.x = Mathf.Clamp(position.x, -2.1f, 2.1f);
        transform.position = position;
    }
	
	void OnCollisionEnter2D(Collision2D collision) {
		if (collision.gameObject.tag.Equals("Enemy Car"))
		{
			//Destroy(gameObject);
            gameObject.SetActive(false);
			audioManager.carSound.Stop();
			uiManager.GameOver();
		}
	}

    void TouchMove()
    {
        if(Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            float middle = Screen.width / 2;
            if(touch.position.x <= middle)
            {
                MoveLeft();
            }
            else if (touch.position.x > middle)
            {
                MoveRight();
            }
            else
            {
                SetVelocityZero();
            }
        }
    }

    public void MoveLeft()
    {
        rigidBody.velocity = new Vector2(-carSpeed, 0);
    }

    public void MoveRight()
    {
        rigidBody.velocity = new Vector2(carSpeed, 0);
    }

    public void SetVelocityZero()
    {
        rigidBody.velocity = Vector2.zero;
    }
}
