﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	public Button[] buttons;
	public Text ScoreText;
	int score; 
	bool gameOver;

	// Use this for initialization
	void Start () {
		score = 0;
		gameOver = false;
		InvokeRepeating("UpdateScore", 1.0f, 0.5f);
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	void UpdateScore() {
		if(gameOver)
			return;
		score++;
		ScoreText.text = "Score:	" + score;
	}
	
	public void GameOver() {
		gameOver = true;
		foreach (Button button in buttons)
		{
			button.gameObject.SetActive(true);
		}
	}
	
	public void Play() {
		Application.LoadLevel("level1");
	}
	
	public void Pause() {
		if(Time.timeScale == 1)
		{
			Time.timeScale = 0;
		}
		else if(Time.timeScale == 0)
		{
			Time.timeScale = 1;
		}
	}
	
	public void Replay() {
		Application.LoadLevel(Application.loadedLevel);
	}
	
	public void Menu() {
		Application.LoadLevel("MenuScene");
	}
	
	public void Exit() {
		Application.Quit();
	}
	
	
}
